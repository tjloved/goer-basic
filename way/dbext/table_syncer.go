package dbext

import (
	"gitee.com/tjloved/goer-basic/way"
	"github.com/pkg/errors"
)

// 数据表同步
type TableSyncer struct {
	columnComparator *ColumnComparator
}

func NewTableSyncer() *TableSyncer {
	return &TableSyncer{columnComparator: &ColumnComparator{}}
}

func (ts *TableSyncer) Sync(originalTable string, originalConnection string, dstTable string, dstConnection string) error {
	if false == way.Db(originalConnection).Migrator().HasTable(originalTable) {
		return errors.Errorf("originalTable %s.%s not exist", originalConnection, originalTable)
	}
	if false == way.Db(dstConnection).Migrator().HasTable(dstTable) {
		var createSqlData map[string]interface{}
		if db := way.Db(originalConnection).Raw("show create table " + originalTable).First(&createSqlData); db.Error != nil {
			return db.Error
		}
		createSqlStr := createSqlData["Create Table"].(string)
		if db := way.Db(dstConnection).Exec(createSqlStr); db.Error != nil {
			return db.Error
		}
	} else {
		ddl := ts.columnComparator.Compare(originalTable, originalConnection, dstTable, dstConnection).ToDDLString()
		if ddl != "" {
			if db := way.Db(dstConnection).Exec(ddl); db.Error != nil {
				return db.Error
			}
		}
	}
	return nil
}
