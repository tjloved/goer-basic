package db

import "gorm.io/gorm/logger"

type Config struct {
	Host      string
	Port      string
	User      string
	Password  string
	Database  string
	Charset   string
	ParseTime string `mapstructure:"parse_time"`
	Loc       string
	Logger    logger.Interface
}

func NewConfig() *Config {
	return &Config{
		ParseTime: "true",
		Loc:       "Local",
		Charset:   "utf8mb4",
	}
}
