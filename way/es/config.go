package es

import "github.com/olivere/elastic/v7"

type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	Gzip     bool
	InfoLog  bool `mapstructure:"info_log"`
	ErrorLog bool `mapstructure:"error_log"`
	TraceLog bool `mapstructure:"trace_log"`
	Logger   elastic.Logger
}

func NewConfig() *Config {
	return &Config{
		Gzip:     true,
		InfoLog:  true,
		ErrorLog: true,
		TraceLog: true,
	}
}
