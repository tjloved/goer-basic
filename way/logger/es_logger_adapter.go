package logger

import "go.uber.org/zap"

type EsZapLoggerAdapter struct {
	Logger *zap.Logger
}

func NewEsZapLoggerAdapter(logger *zap.Logger) *EsZapLoggerAdapter {
	return &EsZapLoggerAdapter{Logger: logger}
}

func (el *EsZapLoggerAdapter) Printf(format string, v ...interface{}) {
	el.Logger.Sugar().Infof(format, v)
}
